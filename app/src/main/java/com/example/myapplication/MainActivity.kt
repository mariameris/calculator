package com.example.myapplication
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.TextView
import java.text.DecimalFormat
import kotlin.Int as Int1

class MainActivity : AppCompatActivity() {
    private lateinit var resultTextView: TextView
    private var operand: Double = 0.0
    private var operation = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        resultTextView = findViewById(R.id.resultTextView)
    }

    fun numberClick(clickedView: View) {
        if (clickedView is TextView) {
            var result = resultTextView.text.toString()
            val number = clickedView.text.toString()
            if (result == "0") {
                result = ""
            }
            resultTextView.text = result + number
        }
    }

    fun operationClick(clickedView: View) {
        if (clickedView is TextView) {
            var operand = resultTextView.text.toString()
            this.operand = operand.toDouble()
            operation = clickedView.text.toString()
            resultTextView.text = ""
        }
    }

    fun equalsClick(clickedView: View) {
        val secOperand = resultTextView.text.toString().toDouble()
        when (operation) {
            "+" -> resultTextView.text = (operand + secOperand).toString()
            "-" -> resultTextView.text = (operand - secOperand).toString()
            "*" -> resultTextView.text = (operand * secOperand).toString()
            "/" -> resultTextView.text = (operand / secOperand).toString()

        }
    }
    fun backSpaceAction(view: View) {
        val length = resultTextView.length()
        if (length > 0)
            resultTextView.text = resultTextView.text.subSequence(0, length - 1)
    }
    fun allClearAction(view: View) {
        operand = 0.0
        resultTextView.text = ""
    }
    fun dotAction(view: View){
        resultTextView.text=resultTextView.text.toString()+"."
    }
}



